$(document).ready(function () {
        var mt = 'margin-top';
        var mb = 'margin-bottom';
        var pt = 'padding-top';
        var pb = 'padding-bottom';
        function pI(elem) {return parseInt(elem)};
        function getH(elem) {return pI($(elem).height())};
        function getMT(elem) {return pI($(elem).css(mt))};
        function getMB(elem) {return pI($(elem).css(mb))};
        function getPT(elem) {return pI($(elem).css(pt))};
        function getPB(elem) {return pI($(elem).css(pb))};

    function sizeVertical(nameElem) {
        var dataElem = getH(nameElem);
          dataElem += getMT(nameElem);
          dataElem += getMB(nameElem);
          dataElem += getPT(nameElem);
          dataElem += getPB(nameElem);
        return dataElem;
    }
    //восота блоков вычисляется тут из фукции sizeVertical(
    var sizeHeader = sizeVertical('.header');
    var sizeMenuButton = sizeVertical('.bigTab__menuButton');
    var paddingTopBigTabContent = getPT('.bigTab__content');
    var sizeH2nthOne = sizeVertical('.blockRight h2');
    var sizeBlockOneH3 = sizeVertical('.blockOne h3');
    var sizeGetHeightOne = sizeVertical('.getHeight.one');
    var sizeGetHeightTwo = sizeVertical('.getHeight.two');
    var sizeBlockOne = sizeVertical('.blockOne');

// блок контрол вычесленных высот
    // console.log(sizeHeader);
    // console.log(sizeMenuButton);
    // console.log(paddingTopBigTabContent);
    // console.log(sizeH2nthOne);
    // console.log(sizeBlockOneH3);
    // console.log(sizeGetHeightOne);
// функция выполняет два действия (подсветка активных пунктов & покатушки левого блока)
    $(window).scroll(function () {
        //первое действие катает блок слевой стороны
        var st = $(this).scrollTop();
        if(st >= 500){
            $('.wrappperList').css({'position':'fixed','top':'17px'});
        }
        if(st >= 1600){
            $('.wrappperList').css({'position': 'absolute', 'top': '1100px'});
        }
        if(st <= 500){
            $('.wrappperList').css({'position': 'relative', 'top': '0px'});
        }
        //блок сложения высот из блока с функцией расчета высоты sizeVertical
        var ulOne = sizeHeader + sizeMenuButton + paddingTopBigTabContent;
        var ulOneLiTwo = ulOne + sizeGetHeightOne + sizeBlockOneH3;
        var ultwo =  ulOne + sizeBlockOne + sizeH2nthOne;
        //сброс параметров
        function reset() {
            $('.wrappperList ul, li').removeClass('active');
        }
        // блок подсвечивает активные пункты меню
        if(st <= ulOne){
            reset()
            $('.wrappperList ul:first-child').addClass('active');
        }
        if(st >= ulOne){
            reset()
            $('.wrappperList ul:nth-child(1) li:first-child').addClass('active');
        }
        if(st >= ulOneLiTwo){
            reset()
            $('.wrappperList ul:nth-child(1) li:nth-child(2)').addClass('active');
        }
        if(st >= ultwo){
            reset()
            $('.wrappperList ul:nth-child(2)').addClass('active');
        }
    });

})
