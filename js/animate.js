$(".wrapperButton .link").hover(
    function () {
        $(this).parent().find(".button").css("animation","down .21s linear forwards");
    },
    function () {
        $(this).parent().find(".button").css("animation","up .21s linear forwards");
    });

$(document).ready(function () {
    $('.languages_list').css('display','none');
});
$('.switch').click(
    function () {
        $('.languages_list').slideToggle('slow');
        $(".switch span").toggleClass('rotate');
    }
);

$("#modal form .button").hover(function () {
    $(this).css("width","180");
    $('body').append('<style id="hubSix">' +
        '#modal form::before{transform:translate(20px)!important; opacity:1!important;}' +
        '#modal form::after{transform:translate(20px)!important; opacity:1!important;}' +
        '</style>');
}, function () {
    $('#hubSix').remove();
    $(this).css("width","143")
});

$(".registrationInput").hover(function () {
    $(this).css("width","210");
    $('body').append('<style id="hubFive">' +
        '.formOne::before{transform:translate(20px)!important; opacity:1!important;}' +
        '.formOne::after{transform:translate(20px)!important; opacity:1!important;}' +
        '</style>');
}, function () {
    $('#hubFive').remove();
    $(this).css("width","170")
});

$(".hoverButton").hover(function () {
    $(this).css("width","270");
    $('body').append('<style id="hubOne">' +
        '.hoverButton:before{transform:translate(20px)!important; opacity:1!important;}' +
        '.hoverButton:after {transform:translate(20px)!important; opacity:1!important;}' +
        '</style>');
}, function () {
    $('#hubOne').remove();
    $(this).css("width","210")
});
$(".blockButton.API .hoverButton").hover(function () {
    $(this).css("width","210");
    $('body').append('<style id="hubTwo">' +
        '.hoverButton:before{transform:translate(20px)!important; opacity:1!important;}' +
        '.hoverButton:after {transform:translate(20px)!important; opacity:1!important;}' +
        '</style>');
}, function () {
    $('#hubTwo').remove();
    $(this).css("width","175")
});
//for arrow page Shop
$("#addShop .blockBottom").hover(function () {
    $(this).find('input').css("width","270");
    $('body').append('<style id="hubThree">' +
        '#addShop .blockBottom:before{transform:translate(20px)!important; opacity:1!important;}' +
        '#addShop .blockBottom:after {transform:translate(20px)!important; opacity:1!important;}' +
        '</style>');
}, function () {
    $('#hubThree').remove();
    $(this).find('input').css("width","210");
});

$("#settings .blockBottom").hover(function () {
    $(this).find('input').css("width","210");
    $('body').append('<style id="hubThree">' +
        '#settings .allsetting::before{transform:translate(20px)!important; opacity:1!important;}' +
        '#settings .allsetting::after {transform:translate(20px)!important; opacity:1!important;}' +
        '</style>');
}, function () {
    $('#hubThree').remove();
    $(this).find('input').css("width","150");
});

$("#settings .blockBottom").hover(function () {
    $(this).find('input').css("width","210");
    $('body').append('<style id="hubThree">' +
        '#settings .security::before{transform:translate(20px)!important; opacity:1!important;}' +
        '#settings .security::after {transform:translate(20px)!important; opacity:1!important;}' +
        '</style>');
}, function () {
    $('#hubThree').remove();
    $(this).find('input').css("width","150");
});

$("#settings .blockBottom").hover(function () {
    $(this).find('input').css("width","210");
    $('body').append('<style id="hubThree">' +
        '#settings .technical::before{transform:translate(20px)!important; opacity:1!important;}' +
        '#settings .technical::after {transform:translate(20px)!important; opacity:1!important;}' +
        '</style>');
}, function () {
    $('#hubThree').remove();
    $(this).find('input').css("width","150");
});
//for arrow page report
$(".blockData__blockSearch .blockData__input").hover(function () {
    $(this).find('input').css("width","170");
    $('body').append('<style id="hubFour">' +
        '.blockData__input:before{transform:translate(20px)!important; opacity:1!important;}' +
        '.blockData__input:after {transform:translate(20px)!important; opacity:1!important;}' +
        '</style>');
}, function () {
    $('#hubFour').remove();
    $(this).find('input').css("width","115");
});

$(window).scroll(function () {//3123 = 100
    var fullHeight = $(this).scrollTop();
    var percent = fullHeight * 10 / 100;
    var hundredPercent = percent * 100 / 3123 * 10 - 63;
    $('.lineLoad .lineLoadIn').css('left', (hundredPercent.toFixed(0) * 5) + '%');
    var load = hundredPercent.toFixed(0) * 3.4;
    if(load >= 0 ){
        $('.circle.1').css('background','rgba(64, 85, 220,.4)');
        $('.circleIn.1').css('background','rgb(64, 85, 220)');
        $('.circle.1').addClass("animationBabl")
    }else{
        $('.circle.1').css('background','rgb(210, 210, 213)');
        $('.circleIn.1').css('background','rgb(210, 210, 213)');
        $('.circle.1').removeClass("animationBabl");
    }
    if(load >= 34 ){
        $('.circle.2').css('background','rgba(64, 85, 220,.4)');
        $('.circleIn.2').css('background','rgb(64, 85, 220)');
        $('.circle.2').addClass("animationBabl")
    }else{
        $('.circle.2').css('background','rgb(210, 210, 213)');
        $('.circleIn.2').css('background','rgb(210, 210, 213)');
        $('.circle.2').removeClass("animationBabl");
    }
    if(load >= 64 ){
        $('.circle.3').css('background','rgba(64, 85, 220,.4)');
        $('.circleIn.3').css('background','rgb(64, 85, 220)');
        $('.circle.3').addClass("animationBabl")
    }else{
        $('.circle.3').css('background','rgb(210, 210, 213)');
        $('.circleIn.3').css('background','rgb(210, 210, 213)');
        $('.circle.3').removeClass("animationBabl");
    }
});
$('.blockCenter div').click(function(){
    $('.blockCenter .qiwi').find('input').fadeOut(20);
    $('.blockCenter div').removeClass('foqusQiwi');
    $('.blockCenter div').removeClass('activePay');
    $('.blockCenter .qiwi').find('input').val('');
    $('.blockCenter .qiwi').find('input').attr('value',' ');

    $('.blockCenter div input').removeAttr("checked");

    $(this).find('input').attr('checked','checked' );
    var z = $(this).find('input').attr("checked");
    if(z == 'checked'){
        console.log(z);
    }else{
        console.log('no checked');
    }
    console.log(z);
    $(this).addClass('activePay');
});
$('.blockCenter .qiwi').click(function() {
    $('.blockCenter div').removeClass('foqusQiwi');
    $('.blockCenter div').removeClass('activePay');
    $(this).addClass('activePay');
    $(this).addClass('foqusQiwi');
    $('.blockCenter .qiwi').find('input').fadeIn(1200);
});
$('.inNav div').bind('click', function () {
    $('.inNav div').removeClass('actives');
    $(this).toggleClass('actives');
});

$('.question > p').click(function () {
    var height = $(this).height();
    $('.question .verticaLine').fadeOut( 100 );
    $(this).parent().find('.verticaLine').fadeIn( 200 );
    $('.question .answer').slideUp(100);
    $(this).next().not(':visible').slideDown(200);
    $(this).parent().find('.verticaLine').css('height',height + 'px');
});
$('.supportHead__supportContent div').hover(
    function () {
        $(this).find('.line').slideDown(210);
        $(this).find('img').css('-webkit-animation','boxShadow .5s ease forwards');
        $(this).find('span').css('color', '#4055dc');
    },
    function () {
        $(this).find('.line').slideUp(50);
        $(this).find('img').css("-webkit-animation","boxShadowNone .35s ease");
        $(this).find('span').css('color', '#33334d');
    });
$('#settings .inNav div').click(function () {
    var spanAttr = $(this).attr('class');
    // console.log(spanAttr);
    var space = spanAttr.indexOf(" ");
    var classNameActiveE = spanAttr.substring(0,space);
    if(classNameActiveE == 'allsetting'){
        $('#settings .modal-body').css('background','url(http://paykassa/img/header/logo background.png) no-repeat 770px center');
        $('.modal-body form').fadeOut();
        $('.modal-body form.allsetting').fadeIn();
    }else if(classNameActiveE == 'technical'){
        $('#settings .modal-body').css('background','url(http://paykassa/img/header/logo background.png) no-repeat 770px center');
        $('.modal-body form').fadeOut();
        $('.modal-body form.technical').fadeIn();
    }else if(classNameActiveE == 'security'){
        $('#settings .modal-body').css('background','none');
        $('.modal-body form').fadeOut();
        $('.modal-body .security').fadeIn();
    }else if(classNameActiveE == 'none'){alert('Страница в разработке')}
});
//shop slide panel
var count = 0;
$('.collapseBlock > div:nth-last-child(1)').fadeOut();
$('.collapseBlock > div:nth-last-child(2)').click(function () {
    $('.collapseBlock > div:nth-last-child(1)').not(this).slideUp(function () {
        $(this).parent().find("div:nth-child(8)").css('transform','rotate(0deg)');
        }
    );
    $(this).parent().find('div:nth-last-child(1)').not(':visible').slideDown(function () {
        $(this).parent().find("div:nth-child(8)").css('transform','rotate(180deg)');
        }
    );
    $('.collapseBlock > div:nth-last-child(2)').css('transform','rotate(0deg)');
});

